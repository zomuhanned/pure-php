Hi Dears, here you will find steps to run the test code.
1- Unpack the zip file. 
2- Copy and Paste the project file in your localhost (htdocs if you are using XAMPP and WWW if you are using Wamp).
3- Run Xampp/Wamp.
4- To run the first task you need to go to this url in your browser "http://localhost/php-test/app/Inventory.php" but first go to the class file "app/Inventory.php" and you can uncomment the function that you want to try. I put examples with brief for each case.
5- To run the unit tests just open terminal in your code editor and run this command "./vendor/bin/phpunit". You can check all test cases, I have created 3 classes, One for each function and inside each one you can check the case study of it.
6- To run the second task, Go to this url in your browser "http://localhost/php-test/index.php" and then the result will be viewed in your web page for all variables you want to show.
Note: Check the port of your local host because you might be changed it before so just check it and add the port.
