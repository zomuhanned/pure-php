<?php

use PHPUnit\Framework\TestCase;
use App\Inventory;


class GetStockTest extends TestCase
{

    public function testGetStock()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "Success",
            "data" => 21,
            "code" => 200
        ];
        $result = $inventory->getStock('Pants');

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testGetStockNotExistType()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "This Type isn't exist yet!",
            "data" => 0,
            "code" => 400
        ];
        $result = $inventory->getStock('Scarfs');

        $this->assertEquals(json_encode($expected), $result);
    }
}
